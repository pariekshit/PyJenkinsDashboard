import MySQLdb.cursors
from flask import Flask, request, url_for,redirect
from flask.templating import render_template




app = Flask(__name__)

database = MySQLdb.connect(host = "localhost", 
    user = "jenkins",
    passwd = "jenkins", 
    db = "jenkins", 
    cursorclass = MySQLdb.cursors.DictCursor)
cursor = database.cursor()

@app.route('/',methods=['GET','POST'])
def login():
    error = None
    if request.method == 'POST':
        if request.form['username'] == 'pari' and request.form['password'] == 'pari':
            return redirect(url_for('graph_example')) 
        else:
            error = "Wrong Credentials"
            
    return render_template('login.html',error=error)
        


@app.route('/dash')
def graph_example(chartID = 'chart_ID', chart_type = 'line', chart_height = 500):
    cursor.execute("SELECT build_number,duration from cellphone")
    data = cursor.fetchall()
    print data
    time = ['0']
    build = []
    for i in data:
        time.append(i['duration'])
        build.append(i['build_number'])
    build = map(int, build)
    time = map(int, time)

    chart = {"renderTo": chartID, "type": chart_type, "height": chart_height,}
    series = [{"name": 'Build number', "data": time}]
    title = {"text": 'Jenkins Job Duration'}
    xAxis = {"categories":''}
    yAxis = {"title": {"text": 'Duration'}}
    return render_template('index.html', chartID=chartID, chart=chart, series=series, title=title, xAxis=xAxis, yAxis=yAxis)

if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)